# coding=utf-8

from flask import Flask, render_template, url_for, flash, redirect, request
from forms import RegistrationForm, LoginForm
from database import createUser, checkUser, getUserById
from user import User

import os
SECRET_KEY = os.urandom(32)

app = Flask(__name__, template_folder='Pages', static_url_path='/static')
app.config['SECRET_KEY'] = SECRET_KEY


def load_user(user_id):
	return getUserById(user_id)

@app.route("/")
def index():
	# , title="Utilizadores" para extender titulo da pagina ex: AAAS - Utilizadores
	return render_template("Home.html", title="Início")


@app.route("/profile", methods=['GET'])
def profile():
	return render_template("Profile.html", title="Perfil")


@app.route("/register", methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('profile'))
	form = RegistrationForm()
	if form.validate_on_submit():
		flash(f"Conta criada para o utilizador { form.username.data }!", 'dark')
		createUser(form.username.data, form.email.data, form.password.data)
		return redirect(url_for('login'))
	return render_template("Register.html", title="Registar", form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('profile'))
	print(request.remote_addr)
	form = LoginForm()
	if form.validate_on_submit():
		user = checkUser(form.email.data, form.password.data)
		if user:
			next_page = request.args.get('next')
			return redirect(next_page) if next_page else redirect(url_for('profile'))
		else:
			flash(f'Email ou password errada!', 'danger')
	return render_template("Login.html", title="Iniciar sessão", form=form)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)

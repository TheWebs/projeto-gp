import pyrebase
from flask_bcrypt import Bcrypt
from user import User

config = {
  "apiKey": "apiKey",
  "authDomain": "projeto-gp-235813.firebaseapp.com",
  "databaseURL": "https://projeto-gp-235813.firebaseio.com",
  "storageBucket": "projeto-gp-235813.appspot.com",
  "serviceAccount": "projeto-gp-235813-firebase-adminsdk-3kd5j-5dcaccf74d.json"
}

bcrypt = Bcrypt()
firebase = pyrebase.initialize_app(config)
db = firebase.database()

#sacar user de val
def getUserFromQuery(query):
	return list(query.items())[0][1]


def createUser(username, email, password):
    user_check = len(db.child("users").order_by_child("email").equal_to(email).get().each())
    if user_check > 0:
    	return False;
    else:
    	last = db.child("users").order_by_child("id").limit_to_last(1).get().val();
    	lastid = getUserFromQuery(last)["id"]
    	user = User(lastid+1, username, email, bcrypt.generate_password_hash(password).decode('utf-8'))
    	db.child("users").push(user.getjsonformat())
    	return True;

def checkUser(email, password):
	#Daqui sai uma lista
	try:
		query_result = db.child("users").order_by_child("email").equal_to(email).get().val()
	except:
		query_result = []
	user_check = len(query_result)
	if user_check > 0:
		user = getUserFromQuery(query_result)
		if bcrypt.check_password_hash(user["hashedpassword"], password):
			return getUser(user)
		else:
			return
	else:
		return


def getUser(user):
	return User(user["id"], user["name"], user["email"], user["hashedpassword"])


def getUserById(id):
	try:
		query_result = db.child("users").order_by_child("id").equal_to(id).get().val()
	except:
		query_result = []
	user_check = len(query_result)
	if user_check > 0:
		user = getUserFromQuery(query_result)
		return getUser(user)
